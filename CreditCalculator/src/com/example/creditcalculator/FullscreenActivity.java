package com.example.creditcalculator;

import com.example.creditcalculator.engine.Calculator;
import com.example.creditcalculator.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = false;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 1500;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = true;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;

    private EditText leftToPayValue;
    private EditText percentageValue;
    private EditText monthlyPaymentValue;

    private TextView yearsValue;
    private TextView monthsValue;
    private TextView restAmountValue;
    private TextView restAmountWithoutPercentageValue;
    private TextView totallySpentValue;
    private TextView lostMoneyValue;

	private Button calculateButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_fullscreen);

//		final View controlsView = findViewById(R.id.fullscreen_content_controls);
		final View contentView = findViewById(R.id.fullscreen_content);

        leftToPayValue = (EditText) contentView.findViewById(R.id.left_to_pay_Value);
        percentageValue = (EditText) contentView.findViewById(R.id.percentage_Value);
        monthlyPaymentValue = (EditText) contentView.findViewById(R.id.monthly_payment_Value);

        yearsValue = (TextView) contentView.findViewById(R.id.years_value);
        monthsValue = (TextView) contentView.findViewById(R.id.months_value);
        restAmountValue = (TextView) contentView.findViewById(R.id.rest_amount_value);
        restAmountWithoutPercentageValue = (TextView) contentView.findViewById(R.id.rest_amount_without_percentage_value);
        totallySpentValue = (TextView) contentView.findViewById(R.id.totally_will_be_spent_value);
        lostMoneyValue = (TextView) contentView.findViewById(R.id.lost_money_value);

//		// Set up an instance of SystemUiHider to control the system UI for
//		// this activity.
//		mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
//		mSystemUiHider.setup();
//		mSystemUiHider
//		.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
//			// Cached values.
//			int mControlsHeight;
//			int mShortAnimTime;
//
//			@Override
//			@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
//			public void onVisibilityChange(boolean visible) {
//				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
//					// If the ViewPropertyAnimator API is available
//					// (Honeycomb MR2 and later), use it to animate the
//					// in-layout UI controls at the bottom of the
//					// screen.
//					if (mControlsHeight == 0) {
//						mControlsHeight = controlsView.getHeight();
//					}
//					if (mShortAnimTime == 0) {
//						mShortAnimTime = getResources().getInteger(
//								android.R.integer.config_shortAnimTime);
//					}
//					controlsView.animate()
//					.translationY(visible ? 0 : mControlsHeight)
//					.setDuration(mShortAnimTime);
//				} else {
//					// If the ViewPropertyAnimator APIs aren't
//					// available, simply show or hide the in-layout UI
//					// controls.
//					controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
//				}
//
//				if (visible && AUTO_HIDE) {
//					// Schedule a hide().
//					delayedHide(AUTO_HIDE_DELAY_MILLIS);
//				}
//			}
//		});

//		// Set up the user interaction to manually show or hide the system UI.
//		contentView.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View view) {
//				if (TOGGLE_ON_CLICK) {
//					mSystemUiHider.toggle();
//				} else {
//					mSystemUiHider.show();
//				}
//			}
//		});

		// Upon interacting with UI controls, delay any scheduled hide()
		// operations to prevent the jarring behavior of controls going away
		// while interacting with the UI.

		calculateButton = (Button)findViewById(R.id.calculate_button);
		calculateButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				calculate();
			}
		});
	}

//	@Override
//	protected void onPostCreate(Bundle savedInstanceState) {
//		super.onPostCreate(savedInstanceState);
//
//		// Trigger the initial hide() shortly after the activity has been
//		// created, to briefly hint to the user that UI controls
//		// are available.
//		delayedHide(100);
//	}


//	/**
//	 * Touch listener to use for in-layout UI controls to delay hiding the
//	 * system UI. This is to prevent the jarring behavior of controls going away
//	 * while interacting with activity UI.
//	 */
//	View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
//		@Override
//		public boolean onTouch(View view, MotionEvent motionEvent) {
//			if (AUTO_HIDE) {
//				delayedHide(AUTO_HIDE_DELAY_MILLIS);
//			}
//			return false;
//		}
//	};
//
//	Handler mHideHandler = new Handler();
//	Runnable mHideRunnable = new Runnable() {
//		@Override
//		public void run() {
//			mSystemUiHider.hide();
//		}
//	};
//
//	/**
//	 * Schedules a call to hide() in [delay] milliseconds, canceling any
//	 * previously scheduled calls.
//	 */
//	private void delayedHide(int delayMillis) {
//		mHideHandler.removeCallbacks(mHideRunnable);
//		mHideHandler.postDelayed(mHideRunnable, delayMillis);
//	}
	
	private void calculate() {
        Calculator calculator = Calculator.getInstance();

        CalculateListener listener = new CalculateListener() {
            @Override
            public void onCalculationIsFinished(final int years, final int months, final float restAmount, final float restAmountWithPercentage, final float totally, final float lostMoney) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        yearsValue.setText(years + "");
                        monthsValue.setText(months + "");
                        restAmountValue.setText(restAmount + "");
                        restAmountWithoutPercentageValue.setText(restAmountWithPercentage + "");
                        totallySpentValue.setText(totally + "");
                        lostMoneyValue.setText(lostMoney + "");
                    }
                });

            }
        };

        calculator.setData( this, Float.parseFloat( leftToPayValue.getText().toString()),
                Float.parseFloat( percentageValue.getText().toString()),
                Float.parseFloat( monthlyPaymentValue.getText().toString()), listener);

        calculator.calculate();
    }

    public interface CalculateListener {

        void onCalculationIsFinished(int years, int months, float restAmount, float restAmountWithPercentage, float totally, float lostMoney);

    }

}
