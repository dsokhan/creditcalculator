package com.example.creditcalculator.engine;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.example.creditcalculator.FullscreenActivity;

public class Calculator {

    private static final String TAG = "Calculator";

	public static Calculator instance;
	
	public static Calculator getInstance() {
		if (instance == null) {
			instance = new Calculator();
		}
		return instance;
	}
	
	private float percentage;
	private float leftToPay;
	private float monthlyPayment;
	
	private int years;
	private int months;
	private float restAmount;
	private float restAmountWithoutPercentage;
	private float leftToPayBaseAmount;
    private float lostMoney;

    private Activity activity;
    private FullscreenActivity.CalculateListener listener;
	
	public void setData(Activity activity, float leftToPay, float percentage, float monthlyPayment, FullscreenActivity.CalculateListener listener) {
        this.activity = activity;

        this.leftToPayBaseAmount = leftToPay;
		this.leftToPay = leftToPay;
		this.percentage = percentage;
		this.monthlyPayment = monthlyPayment;

        this.listener = listener;
	}
	
	public void calculate(){
        int totallyWillBeSpent = 0;
        months = 0;
        while ( leftToPay > 0 ){
            if (leftToPay < monthlyPayment) {
                restAmount = leftToPay;
                break;
            }
            months++;
            totallyWillBeSpent += monthlyPayment;
            leftToPay = calculateBodyAmount();
            Log.i(TAG, "months = " + months);
            Log.i(TAG, "leftToPay = " + leftToPay);
        }

        years = (int) Math.floor( months / 12 );
        months = months % 12;
        lostMoney = totallyWillBeSpent - leftToPayBaseAmount;
        listener.onCalculationIsFinished(years, months, restAmount, 0, totallyWillBeSpent, lostMoney );
        Log.i(TAG, "restAmount = " + restAmount);
        Toast.makeText(activity, "Calculation is finished", Toast.LENGTH_LONG).show();
	}

    private float calculateMonthlyCreditPercentage() {
        float result = 0.0f;

        result = ( ( leftToPay / 100 ) * percentage ) / 12;

        return result;
    }

    private float calculateBodyAmount () {
        float result = 0.0f;

        float body = monthlyPayment - calculateMonthlyCreditPercentage();

        result = leftToPay - body;

        return result;
    }


}
